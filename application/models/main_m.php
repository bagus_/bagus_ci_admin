<?php

class main_m extends CI_Model{
    function __construct(){
        parent::__construct();
    }
	
	function index(){
	
	}
	
	function get_data_mhs(){
		$query = 'SELECT * FROM data_mhs';
		$this->db->limit(10);
		return $this->db->query($query);

	}

	function get_data_antrian(){

		//select data from view 'view_antrian_mhs'
		//view was create from two table
		//table 'log_antrian' and 'data_mhs'
		//$this->db->select('npm', 'id_antrian', 'nama', 'jenis_surat');
		$this->db->from('view_antrian_mhs');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			# code...
			return $query;
		}
		return 0;
	}

	function count_kp(){

		$this->db->where('jenis_surat', 'kp');
		$query = $this->db->get('log_antrian');
		if ($query->num_rows() > 0) {
			# code...
			return $query->num_rows();
		}
		return 0;
	}
	function count_skripsi(){

		$this->db->where('jenis_surat', 'skripsi');
		$query = $this->db->get('log_antrian');
		if ($query->num_rows() > 0) {
			# code...
			return $query->num_rows();
		}
		return 0;
	}
	function count_survey(){

		$this->db->where('jenis_surat', 'survey');
		$query = $this->db->get('log_antrian');
		if ($query->num_rows() > 0) {
			# code...
			return $query->num_rows();
		}
		return 0;
	}
	function count_sk(){

		$this->db->where('jenis_surat', 'sk');
		$query = $this->db->get('log_antrian');
		if ($query->num_rows() > 0) {
			# code...
			return $query->num_rows();
		}
		return 0;
	}
    
}
