

<?php

//we need to start session in order to access it through CI

Class User_Authentication extends CI_Controller {

	public function __construct() {
		parent::__construct();

		// Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');

		// Load form validation library
		$this->load->library('form_validation');

		// Load session library
		//$this->load->library('session');

		// Load database
		$this->load->model('login_database');
	}

	// Show login page
	public function index() {
		$this->load->view('index');
	}

	// Show registration page
	public function user_profile_show() {

		$npm = $this->session->userdata['logged_in']['npm'];
		$data['query']= $this->login_database->read_user_information($npm);
		$this->load->view('profile_form',$data);
	}

	// Validate and store registration data in database
	public function user_edit() {

		// Check validation for user input in SignUp form
		$this->form_validation->set_rules('nama','Nama', 'trim|required|xss_clean');
		$this->form_validation->set_rules('tempat','Tempat', 'trim|required|xss_clean');
		$this->form_validation->set_rules('tanggal','Tanggal', 'trim|required|xss_clean');
		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required|xss_clean');
		$this->form_validation->set_rules('telp', 'Telp', 'trim|required|xss_clean');
		$this->form_validation->set_rules('alamat_or', 'Alamat_or', 'trim|required|xss_clean');
		$this->form_validation->set_rules('pkr_or', 'Pkr_or', 'trim|required|xss_clean');
		$this->form_validation->set_rules('telp_or', 'Telp_or', 'trim|required|xss_clean');

		if ($this->form_validation->run() == FALSE) {
			$data['message_display'] = 'somthing error!';
			$this->load->view('profile_form', $data);
		} else {
			$npm = $this->input->post('npm');
			$result = $this->login_database->update_mhs($npm);
			if ($result == TRUE) {
				redirect('main','refresh');
			} else {
				$data['message_display'] = 'update gagal!!!';
				$this->load->view('profile_form', $data);
			}
		}
	}

	// Check for user login process
	public function user_login_process() {

		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
		//$this->form_validation->set_rules('npm', 'Npm', 'trim|required|xss_clean');

		if ($this->form_validation->run() == FALSE) {
			if(isset($this->session->userdata['logged_in'])){
				redirect('main','refresh');
			}else{
				$this->load->view('index');
			}
		} else {
			$data = array(
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password')
			);
			//$data = array('npm' => $this->input->post('npm') );
			$result = $this->login_database->login($data);
			if ($result == TRUE) {

				//$username = $this->input->post('username');
				//$npm = $this->input->post('npm');
				//$result = $this->login_database->read_user_information($npm);
				if ($result != false) {
					
						$session_data = array(
							'username' => $result->username,
							'email' => $result->password
							/*'npm' => $result->npm,
							'nama' => $result->nama,
							'tanggal' => $result->tanggal,
							'telp' => $result->telp,
							'alamat' => $result->alamat,
							'tempat' => $result->tempat,
							'alamat_or' => $result->alamat_or,
							'pkr_or' => $result->pkr_or,
							'telp_or' => $result->telp_or,
							'skripsi' => $result->skripsi*/
						);
						// Add user data in session
						$this->session->set_userdata('logged_in', $session_data);
						//$this->login_database->save_log_user('login');
					
						redirect('index.php/main','refresh');
					
					
					}
				} else {
					$data = array(
					'error_message' => 'Invalid Username or Password'
					);
					$this->load->view('index', $data);
			}
		}
	}

	// Logout from admin page
	public function logout() {

	// Removing session data
		$sess_array = array(
		'npm' => ''
		);
		$this->load->model('login_database');
    	$result = $this->login_database->save_log_user('logout');

	    if ($result==TRUE) {
	    	$this->session->unset_userdata('logged_in', $sess_array);
			$data['message_display'] = 'Successfully Logout';
			redirect('index/logout_user','refresh');
	    } else {
	    	redirect('main','refresh');
	    }
	    	
	}

}

?>

