<!DOCTYPE html>
<html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>ITATS - Jurusan Teknik Informatika</title>

        <meta name="description" content="ProUI is a Responsive Bootstrap Admin Template created by pixelcave and published on Themeforest.">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="<?php echo base_url();?>assets/img/favicon.ico">
        <link rel="apple-touch-icon" href="<?php echo base_url();?>assets/img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="<?php echo base_url();?>assets/img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="<?php echo base_url();?>assets/img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="<?php echo base_url();?>assets/img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="<?php echo base_url();?>assets/img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="<?php echo base_url();?>assets/img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="<?php echo base_url();?>assets/img/icon152.png" sizes="152x152">
        <!-- END Icons -->

		<!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="<?= site_url('assets/css/bootstrap.min.css') ?>">

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="<?= site_url('assets/css/plugins.css') ?>">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="<?= site_url('assets/css/main.css')?>">

        <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="<?= site_url('assets/css/themes.css') ?>">
        <!-- END Stylesheets -->

        <!-- Modernizr (browser feature detection library) & Respond.js (Enable responsive CSS code on browsers that don't support it, eg IE8) -->
        <script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
        
        <script type="text/javascript" language="javascript" src="<?php echo base_url('assets/js/pages/tablesDatatables') ?>" ></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/datatables/media/css/jquery.dataTables.css">
        
        <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>vendor/datatables/media/js/jquery.dataTables.js"></script>
         
        
    </head>
    <!-- In the PHP version you can set the following options from inc/config file -->
    <!--
        Available body classes:

        'page-loading'      enables page preloader
    -->
    <body>
  

<?php $this->load->view('template/sidebar'); ?>
                <!-- Page content -->
             
        
        
             <!-- Page content -->
        <div id="page-content">
                
                    <!-- Dashboard Header -->
                    <!-- For an image header add the class 'content-header-media' and an image as in the following example -->
                    <div class="content-header content-header-media">
                        <div class="header-section">
                            <div class="row text-center">
                                <!-- Main Title (hidden on small devices for the statistics to fit) -->
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <h1> <strong>Selamat Datang </strong>Admin<br></h1>
                                </div>
                                <!-- END Main Title -->
                            </div>
                        </div>
                        <!-- For best results use an image with a resolution of 2560x248 pixels (You can also use a blurred image with ratio 10:1 - eg: 1000x100 pixels - it will adjust and look great!) -->
                        <img src="<?php echo base_url(); ?>assets/img/placeholders/headers/dashboard_header.jpg" alt="header image" class="animation-pulseSlow">
                    </div>
                    <!-- END Dashboard Header -->


                    <br><br>
            <!-- Datatables Header -->
                    <div class="content-header">
                        <div class="header-section">
                            <h1>
                                <i class="fa fa-table"></i>Datatables<br><small>HTML tables can become fully dynamic with cool features!</small>
                            </h1>
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Tables</li>
                        <li><a href="">Datatables</a></li>
                    </ul>
                    <!-- END Datatables Header -->
                    
                    <!-- Datatables Content -->
                     <!-- Datatables Content -->
                   <div class="row">
                         <div class="block full">
                        <div class="block-title">
                            <h2><strong>Datatables</strong> integration</h2>
                        </div>
                        <div class="table-responsive">
                            <table id="example-datatable" class="table table-vcenter table-condensed table-bordered table-striped">
                                <thead>
                                    <tr>
                                         <th class="text-center"> kode antrian </th>
                                        <th class="text-center"> No. Surat </th>
                                        <th class="text-center"> NPM </th>
                                        <th class="text-center"> Nama </th>
                                        <th class="text-center"> Keperluan </th>
                                        <th class="text-center"> Actions </th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                               // $no = 1;
                                foreach ($query->result() as $item):?>
                                <tr>
                                    <td class="text-center"><?= $item->id_antrian;?></td>
                                    <td class="text-center"><?= $item->no_surat;?></td>
                                    <td class="text-center"><?= $item->npm;?></td>
                                    <td><?= $item->nama;?></td>
                                    <td class="text-center"><?= $item->jenis_surat;?></td>
                                    <td class="text-center">
                                            <div class="btn-group">
                                                <a href="<?= base_url(); ?>main/submit/nomor<?= $item->no_surat; ?>" data-toggle="tooltip" title="Aprove" class="btn btn-lg btn-warning">terima</i></a>
                                                <a href="javascript:void(0)" data-toggle="tooltip" title="Aprove" class="btn btn-lg btn-success disabled"><i class="gi gi-ok_2"></i></a>
                                                <a href="<?= base_url(); ?>word/<?= $item->jenis_surat;?>?id=<?= $item->id_antrian;?>" data-toggle="modal" class="btn btn-lg btn-primary">Cetak</i></a>
                                                <a href="javascript:void(0)" data-toggle="tooltip" title="Delete" class="btn btn-lg btn-danger"><i class="gi gi-remove_2"></i></a>
                                            </div>
                                    </td>
                                </tr>
                                <?php
                                    //$no++;
                                endforeach;?>
                                </tbody>
                             </table>
                        </div>                    
                    </div>
                   </div>
                    <!-- END Datatables Content -->

        </div>
        <!-- END Page Content -->


        <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
        <div id="modal-user-settings" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header text-center">
                        <h2 class="modal-title"><i class="fa fa-pencil"></i> Settings</h2>
                    </div>
                    <!-- END Modal Header -->

                    <!-- Modal Body -->
                    <div class="modal-body">
                        <form action="index.html" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" onsubmit="return false;">
                            <fieldset>
                                <legend>Vital Info</legend>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Username</label>
                                    <div class="col-md-8">
                                        <p class="form-control-static">Admin</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-email">Email</label>
                                    <div class="col-md-8">
                                        <input type="email" id="user-settings-email" name="user-settings-email" class="form-control" value="admin@example.com">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-notifications">Email Notifications</label>
                                    <div class="col-md-8">
                                        <label class="switch switch-primary">
                                            <input type="checkbox" id="user-settings-notifications" name="user-settings-notifications" value="1" checked>
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend>Password Update</legend>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-password">New Password</label>
                                    <div class="col-md-8">
                                        <input type="password" id="user-settings-password" name="user-settings-password" class="form-control" placeholder="Please choose a complex one..">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-repassword">Confirm New Password</label>
                                    <div class="col-md-8">
                                        <input type="password" id="user-settings-repassword" name="user-settings-repassword" class="form-control" placeholder="..and confirm it!">
                                    </div>
                                </div>
                            </fieldset>
                            <div class="form-group form-actions">
                                <div class="col-xs-12 text-right">
                                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-sm btn-primary">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END Modal Body -->
                </div>
            </div>
        </div>
        <!-- END User Settings -->
<?php $this->load->view('template/footbar'); ?>

         <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file (Remove 'http:' if you have SSL) -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>

        <!-- Bootstrap.js, Jquery plugins and Custom JS code -->
        <script src="<?= site_url('assets/js/vendor/bootstrap.min.js')?>"></script>
        <script src="<?= site_url('assets/js/plugins.js') ?>"></script>
        <script src="<?= site_url('assets/js/app.js') ?>"></script>

        <!-- Load and execute javascript code used only in this page -->
        <script src="<?= site_url('assets/js/pages/tablesDatatables.js') ?>"></script>
        <script>$(function(){ TablesDatatables.init(); });</script>
    </body>
</html>