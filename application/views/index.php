<?php   $this->load->view("template/header"); ?>
<!-- Login Alternative Row -->
<div class="container">
    <div class="row">
        <div class="col-md-5 col-md-offset-1">
            <div id="login-alt-container">
                <!-- Title -->
                <h1 class="push-top-bottom">
                    <i class="gi gi-imac"></i> <strong>Teknik Informatika</strong><br>
                        <small>INSTITUT TEKNOLOGI ADHI TAMA SURABAYA</small>
                </h1>
                <!-- END Title -->

                <!-- Footer -->
                <footer class="text-muted push-top-bottom">
                    <small><span id="year-copy"></span> &copy; <a target="_blank">Admin</a></small>
                </footer>
                <!-- END Footer -->
            </div>
        </div>
        <div class="col-md-6">
            <!-- Login Container -->
            <div id="login-container" class="animation-fadeIn">
                <!-- Login Title -->
                <div class="login-title text-center">
                    <h1><strong>Login</strong></h1>
                </div>
                <!-- END Login Title -->
                <?php
                if (isset($logout_message)) {
                    echo "<div class='message'>";
                    echo $logout_message;
                    echo "</div>";
                }
           
                if (isset($message_display)) {
                    echo "<div class='message'>";
                    echo $message_display;
                    echo "</div>";
                }
            ?>
                <!--
                <!-- Login Block -->
                <div class="block push-bit">
                    <div id="form-login" class="form-horizontal">
                    <!-- Login Form -->
                    <!--<form action="<?php echo base_url(); ?>user_authentication/user_login_process" method="post" id="form-login" class="form-horizontal" method="POST">
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                       --> 
                        <?php echo form_open('index.php/user_authentication/user_login_process'); ?>
                        <?php
                            echo "<div class='error_msg'>";
                            if (isset($error_message)) {
                            echo $error_message;
                            }
                            echo validation_errors();
                            echo "</div>";
                        ?> 
                       <div class="form-group">
                            <div class="col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                        <input type="text" class="form-control input-lg" name="username" id="username" placeholder="Username" maxlength="100" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                                        <input type="password" class="form-control input-lg" id="login-password" name="password" placeholder="Password" maxlength="999" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-xs-4">
                                <label class="switch switch-primary" data-toggle="tooltip" title="Remember Me?">
                                    <input type="checkbox" id="login-remember-me" name="login-remember-me" checked>
                                        <span></span>
                                </label>
                            </div>
                            <div class="col-xs-8 text-right">
                                <button type="submit" name="btnLogin" value="admin" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Login</button>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12 text-center">
                                <a href="<?= base_url() ?>forgot" id="link-reminder-login"><small>Forgot password?</small></a>
                            </div>
                        </div>
                   <!-- </form>-->
                    <!-- END Login Form -->  
                     <?php echo form_close(); ?>   
                     </div>                       
                </div>
                <!-- END Login Block -->
            </div>
            <!-- END Login Container -->
        </div>
    </div>
</div>
<!-- END Login Alternative Row -->
<?php   $this->load->view("template/footer"); ?>	
        